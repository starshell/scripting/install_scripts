# Install VIM

Installs Vim 8 with Python3 support on Debian based systems.

## Note

Debian 8 (Stretch) and Ubuntu 18.04 (Bionic) come with Vim 8 so this script is not needed.
