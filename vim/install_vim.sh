#!/bin/bash

source "../posix_common/posix_common.sh"

if ! _ask_yes_no "Install latest vim from source"; then
    echo ''
    exit 0
fi

# Package Management {{{
_info "Installing dependencies"
sudo apt-get install libncurses5-dev \
                     libgnome2-dev \
                     libgnomeui-dev \
                     libgtk2.0-dev \
                     libatk1.0-dev \
                     libbonoboui2-dev \
                     libcairo2-dev \
                     libx11-dev \
                     libxpm-dev \
                     libxt-dev \
                     python-dev \
                     python3-dev \
                     ruby-dev \
                     lua5.1 \
                     libtolua-dev \
                     libtolua++5.1-dev \
                     liblua5.1-dev \
                     luajit \
                     libluajit-5.1 \
                     libluajit-5.1-dev \
                     libperl-dev \
                     libcairo2-dev

_info "Installation will not work if vim is already installed."
if _ask_yes_no "Remove vim from the system"; then
    sudo apt-get remove vim \
                        vim-runtime \
                        gvim
                        vim-tiny \
                        vim-common \
                        vim-gui-common
fi
# End Package Management }}}
# Compile vim from source {{{

_info "Fetching vim source from git"
git clone https://github.com/vim/vim.git
cd vim

_info "Building vim"
./configure --with-features=huge \
            --enable-gui=gtk2 \
            --enable-multibyte \
            --enable-largefile \
            --enable-luainterp=yes \
            --with-luajit \
            --enable-pythoninterp=no \
            --enable-python3interp=yes \
            --enable-rubyinterp=yes \
            --enable-perlinterp=yes \
            --enable-cscope

make VIMRUNTIMEDIR=/usr/share/vim/vim81
sudo make install
cd ..
rm -rf vim
# End compile vim }}}
# Update default editor {{{
if _ask_yes_no "Set vim as default editor"; then
    sudo update-alternatives --install /usr/bin/editor editor /usr/bin/vim 1
    sudo update-alternatives --set editor /usr/bin/vim
    sudo update-alternatives --install /usr/bin/vi vi /usr/bin/vim 1
    sudo update-alternatives --set vi /usr/bin/vim
fi
# End update default editor }}}

_info "Vim successfully installed"
exit 0
